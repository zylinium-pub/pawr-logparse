"""test collab"""
# pylint:disable=import-error,wrong-import-order,wrong-import-position

RESERVATION = 114324
RESERVATION_TYPE = "colosseum"


def test_all(use_test_config):
    """test collab object"""
    from pawrlog.enb import ENB

    ENB(RESERVATION, RESERVATION_TYPE).run()
