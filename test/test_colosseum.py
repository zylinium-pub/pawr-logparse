"""test collab"""
# pylint:disable=import-error,wrong-import-order,wrong-import-position

RESERVATION = 115207
RESERVATION_TYPE = "colosseum"


def test_all(use_test_config):
    """test collab object"""
    from pawrlog.traffic import Traffic
    from pawrlog.enb import ENB
    from pawrlog.slack import Slack
    from pawrlog.collab import Collab
    from pawrlog.radio_conf import RadioConf
    from pawrlog.elastic import Elastic
    from pawrlog.metrics import Metrics

    Collab(RESERVATION, RESERVATION_TYPE).run()
    Traffic(RESERVATION, RESERVATION_TYPE).run()
    ENB(RESERVATION, RESERVATION_TYPE).run()
    RadioConf(RESERVATION, RESERVATION_TYPE).run()
    Metrics(RESERVATION, RESERVATION_TYPE).run()
    Slack(RESERVATION, RESERVATION_TYPE).run()
    Elastic(RESERVATION, RESERVATION_TYPE).run()
