"""test collab"""
# pylint:disable=import-error,wrong-import-order,wrong-import-position


def test_all(use_test_config):
    """test collab object"""
    from pawrlog.grok_patterns import RBG_PATTERN

    test_lines = """1606848470870185 [PHY1] [I] [ 5170] PDCCH: cc=0, f=1, cce=14, L=1, rbg=0x1000000, pid=0, mcs={12}, ndi={0}, tpc_pucch=1, tti_tx_dl=5174
1606848471310218 [PHY1] [I] [ 5610] PDCCH: cc=0, f=1, cce=10, L=1, rbg=0x1000000, pid=0, mcs={12}, ndi={0}, tpc_pucch=1, tti_tx_dl=5614
1606848471330153 [PHY1] [I] [ 5630] PDCCH: cc=0, f=1, cce= 4, L=1, rbg=0x1000000, pid=0, mcs={12}, ndi={0}, tpc_pucch=1, tti_tx_dl=5634""".split(
        "\n"
    )
    print(test_lines)

    for line in test_lines:
        match = RBG_PATTERN.match(line)
        print(match)
