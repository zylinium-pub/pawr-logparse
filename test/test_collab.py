"""test collab"""
# pylint:disable=import-error,wrong-import-order,wrong-import-position

RESERVATION = 202103221127
RESERVATION_TYPE = "powder"


def test_all(use_test_config):
    """test collab object"""
    from pawrlog.collab import Collab

    Collab(RESERVATION, RESERVATION_TYPE).run()
