"""test setup"""
import sys
from pathlib import Path


def setup():
    """add dags to path"""
    module_path = Path(__file__).resolve().parent.parent / "dags"
    sys.path.insert(0, str(module_path))
