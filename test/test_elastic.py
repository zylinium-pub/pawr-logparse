"""test elastic"""
# pylint:disable=import-error,wrong-import-order,wrong-import-position

RESERVATION = 202103251309
RESERVATION_TYPE = "powder"


def test_elastic(use_test_config):
    """test elastic object"""
    from pawrlog.elastic import Elastic

    Elastic(RESERVATION, RESERVATION_TYPE).run()
