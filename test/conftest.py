"""pytest common functions"""
# pylint:disable=import-error,wrong-import-order,wrong-import-position
from .dir_setup import setup

from pytest import fixture
@fixture
def use_test_config(monkeypatch):
    """set up test variables"""
    setup()
    from pawrlog.config import TEST_SLACK_URL, TEST_SLACK_DELAY, TEST_ES_INDEX

    monkeypatch.setattr("pawrlog.config.SLACK_URL", TEST_SLACK_URL)
    monkeypatch.setattr("pawrlog.config.SLACK_DELAY", TEST_SLACK_DELAY)
    monkeypatch.setattr("pawrlog.config.ES_INDEX", TEST_ES_INDEX)
    return None
