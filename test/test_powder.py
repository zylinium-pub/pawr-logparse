"""test collab"""
# pylint:disable=import-error,wrong-import-order,wrong-import-position

RESERVATION = 202104131905
RESERVATION_TYPE = "powder"


def test_all(use_test_config):
    """test collab object"""
    from pawrlog.traffic import Traffic
    from pawrlog.enb import ENB
    from pawrlog.slack import Slack

    # from pawrlog.elastic import Elastic

    Traffic(RESERVATION, RESERVATION_TYPE).run()
    ENB(RESERVATION, RESERVATION_TYPE).run()
    Slack(RESERVATION, RESERVATION_TYPE).run()
    # Elastic(RESERVATION, RESERVATION_TYPE).run()
