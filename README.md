# pawr-logparse

Airflow infrastructure for parsing logs from Colossuem and POWDER.

The main processing pipeline DAG is defined in `dags/process_reservation.py`.
This reservation processsing pipeline is cued by the two `get_reservation` DAGs
which poll Colosseum and POWDER jobs to see if there are any new jobs to
process.  When a completed reservation is found on either platform, the
`process_reservation` DAG kicks in to process the output files.


## Deploy Airflow

To deploy this infrastructure, you will need `docker-compose` installed
locally.  You also need your Colosseum ssh key as Airflow uses ssh to pull
files from Colosseum.  Once docker-compose successfully runs, the
`get_reservation` DAGs will look over the last hour to find any completed
reservations as well as continue to poll Colossuem into the future as new
reservations are completed.

Here are the steps to deploy this processing infrastructure:

- Add a `colossuem` key to the `.ssh` folder in this repo. This key will need
   to be in the `authorized_keys` list in your Colosseum File Proxy such that
   it allows for password-free ssh access to Colosseum.
- Fill in the credentials in `docker-compose` and `dags/pawrlog/config.py`.
- Create a creds.json auth file from Google Cloud Storage if you want to
  support uploading plot artifacts to a GS storage bucket.  If you do not want
  to do this, delete the line from Dockerfile that copies this file to the
  Docker container.
- Deploy Airflow via docker compose: `docker-compose up -d --build`. This will
   keep Airflow running even after restarts.
- Ensure Airflow has ssh access: `docker exec -it pawrlog_worker_1 ssh
   sc2-lz`.   If that command does not work, then Airflow will not be able to copy files from
Colossuem.
- Airflow interface will be at `http://localhost:51511`.



## Development

The `dags` folder contains the `pawrlog` library.  This library gets copied to
the Airflow containers and defined the DAG processing pipelines.  The
`ReservationJob` base class is useful for creating DAG jobs and can be extended
by simply defining a `run` function that Airflow will run as part of the DAG.

Docker has this local folder mounted locally as a volume (see the
`docker-compose` file), so any changes you
make to this file locally will be reflected in the running Airflow instance.

`ReservationJobs` can be easily tested locally and independently of Airflow
using pytest.  See `tests` folder for examples.  In order to have these tests
run locally, you will have to install the `config/requirements.txt` using `pip
install -r config/requirements.txt`.

For example:
```
python -m pytest -sv test/test_grok.py
```

Pytest will monkey patch the "TEST" variables in the pawrlog library before
running a test.  This let's you send test results to a different slack channel
than reservation runs, for instance.
