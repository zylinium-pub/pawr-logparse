"""
Code that goes along with the Airflow located at:
http://airflow.readthedocs.org/en/latest/tutorial.html
"""
from datetime import timedelta, datetime

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.exceptions import AirflowSkipException

from pawrlog.colosseum_api import get_batch_reservations_, rsync_reservations_
from pawrlog.airflow import (
    get_batch_reservations,
    get_conditional_sub_dags,
)
from pawrlog.config import POLL_INTERVAL


dag = DAG(
    "get_reservations_colosseum",
    default_args={
        "start_date": datetime.utcnow() - timedelta(hours=12),
    },
    schedule_interval=timedelta(minutes=POLL_INTERVAL),
    is_paused_upon_creation=False,
)

t_print_date = BashOperator(task_id="print_date", bash_command="date", dag=dag)


t_get_batch_reservations = PythonOperator(
    task_id="get_batch_reservations",
    python_callable=get_batch_reservations,
    dag=dag,
    op_kwargs={"get_batch_function": get_batch_reservations_},
    provide_context=True,
)


def rsync_reservations(**context):
    """rsync reservation list"""
    # grab context
    _, res_ids = context["ti"].xcom_pull(task_ids="get_batch_reservations")
    if res_ids:
        rsync_reservations_(res_ids)
    else:
        raise AirflowSkipException("Skipping Rsync. No files to sync")


t_rsync_reservations = PythonOperator(
    task_id="rsync_reservations",
    python_callable=rsync_reservations,
    dag=dag,
    provide_context=True,
)


t_subs = get_conditional_sub_dags(dag)

t_print_date >> t_get_batch_reservations >> t_rsync_reservations >> t_subs  # pylint: disable=W0104

if __name__ == "__main__":
    dag.cli()
