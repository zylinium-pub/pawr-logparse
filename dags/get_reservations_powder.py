"""
Code that goes along with the Airflow located at:
http://airflow.readthedocs.org/en/latest/tutorial.html
"""
from typing import List
from datetime import timedelta, datetime

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

from pawrlog.airflow import (
    get_batch_reservations,
    get_conditional_sub_dags,
)
from pawrlog.config import POLL_INTERVAL, POWDER_PATH


dag = DAG(
    "get_reservations_powder",
    default_args={
        "start_date": datetime.utcnow() - timedelta(hours=12),
    },
    schedule_interval=timedelta(minutes=POLL_INTERVAL),
    is_paused_upon_creation=False,
)


t_print_date = BashOperator(task_id="print_date", bash_command="date", dag=dag)


def get_batch_reservations_powder(start_time: float, stop_time: float) -> List[str]:
    """poll powder folder to see what new reservations have completed"""

    # loop through reservations and keep any that are in our time interval.
    res_ids = []
    for completed in POWDER_PATH.glob("**/rf_stop_time.json"):
        res_number = completed.parent.name.split("-")[-1]
        res_time = completed.stat().st_ctime
        if start_time <= res_time < stop_time:
            res_ids.append(res_number)
    return "powder", res_ids


t_get_batch_reservations = PythonOperator(
    task_id="get_batch_reservations",
    python_callable=get_batch_reservations,
    dag=dag,
    op_kwargs={"get_batch_function": get_batch_reservations_powder},
    provide_context=True,
)


t_subs = get_conditional_sub_dags(dag)

t_print_date >> t_get_batch_reservations >> t_subs  # pylint: disable=W0104

if __name__ == "__main__":
    dag.cli()
