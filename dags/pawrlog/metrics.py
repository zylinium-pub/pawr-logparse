"""process metrics logs"""
import json
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

from .core import ReservationJob, plotting, jsoning

matplotlib.use("Agg")


class Metrics(ReservationJob):
    """process metrics log files"""

    def __init__(self, *args, **kwargs):
        self.df_dict = None
        self.files = [
            {"name": "enb_metrics.csv", "min_time": 1, "groupby": None},
            {"name": "enb_metrics_ues.csv", "min_time": 30, "groupby": "ue_name"},
            {"name": "ue_metrics.csv", "min_time": 3000, "groupby": "srn"},
        ]
        super().__init__(*args, **kwargs)

    def load_data(self):
        """load all data into objejct memory"""
        self._get_metric_data()

    def _get_metric_data(self):
        """get metric csvs"""
        out = {}
        for file_data in self.files:
            metrics = list(self.reservation_directory.glob(f"**/{file_data['name']}"))
            print(file_data, self.reservation_directory, metrics)
            dfs = []
            for metric in metrics:
                df = pd.read_csv(
                    metric, delimiter=";", error_bad_lines=False, warn_bad_lines=True
                )[:-1]
                df.time = df.time.astype(float)
                df["srn"] = str(metric.parent.name).split("-")[-2]
                dfs.append(df)
            if dfs:
                file_data["df"] = pd.concat(dfs)

    @plotting
    def _plot_metrics(self):
        """plot UE metrics"""
        cols_of_interest = ["n", "sinr", "dl_snr", "rsrp", "rssi"]
        for file_data in self.files:
            if "df" not in file_data:
                continue
            df = file_data["df"]
            for col in df:
                if col not in cols_of_interest:
                    continue
                fgr = sns.FacetGrid(
                    df[df.time > file_data["min_time"]],
                    hue=file_data["groupby"],
                    aspect=4,
                )
                fgr.map(plt.plot, "time", col, marker=".", ls="").add_legend()
                plt.grid(True)
                yield (f"{file_data['name']}_{col}", fgr)

    def run(self):
        """run this job"""
        self.load_data()
        self._plot_metrics()
