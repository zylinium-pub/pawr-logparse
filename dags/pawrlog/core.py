"""Base class for reservation runners"""
from __future__ import annotations
from typing import Type
from abc import ABCMeta, abstractmethod
from pathlib import Path
import functools
from inspect import isgeneratorfunction
import json

from airflow.operators.python_operator import PythonOperator
import matplotlib
import matplotlib.pyplot as plt
import slackweb
import seaborn as sns
import pandas as pd
from tabulate import tabulate

from .config import SLACK_URL, POWDER_PATH

matplotlib.use("Agg")


def run_job(reservation_job: Type[ReservationJob], **context) -> None:
    """Run a job by instantiating a job instance and calling run()"""
    reservation_number = int(context["dag_run"].conf["reservation"])
    reservation_type = context["dag_run"].conf["reservation_type"]
    reservation_job(reservation_number, reservation_type).run()


def plotting(plot_function):
    """plotting decorator"""

    @functools.wraps(plot_function)
    def magic(self):
        if isgeneratorfunction(plot_function):
            self.setup_sns()
            for sub_name, fgr in plot_function(self):
                name = f"{self.class_name()}_{plot_function.__name__}_{sub_name}"
                self.save_sns(fgr, name)
        else:
            self.setup_sns()
            fgr = plot_function(self)
            name = f"{self.class_name()}_{plot_function.__name__}"
            self.save_sns(fgr, name)

    return magic


def jsoning(json_function):
    """json making decorator"""

    @functools.wraps(json_function)
    def magic(self):
        data_dict = json_function(self)
        name = f"{self.class_name()}_{json_function.__name__}"

        file_name = f"{name}_{self.reservation_name}.json"

        path = self.output_directory / file_name
        print(f"saving json to {path}")
        json.dump(data_dict, path.open("w"))

    return magic


def tabling(table_function):
    """table making decorator"""

    @functools.wraps(table_function)
    def magic(self):
        df = table_function(self)
        name = f"{self.class_name()}_{table_function.__name__}"
        self.write_string_to_file(
            tabulate(
                df,
                headers="keys",
            ),
            name,
            "md",
        )

    return magic


class ReservationJob(metaclass=ABCMeta):
    """base class for reservation jobs"""

    def __init__(self, reservation_number: int, reservation_type: str):
        self.reservation_number = int(reservation_number)
        self.reservation_type = reservation_type
        self.radio_name_map = {}
        if reservation_type == "colosseum":
            self.reservation_name = f"RESERVATION-{self.reservation_number}"
            self.reservation_directory = Path(f"/data/pawr/{self.reservation_name}")
            self.output_url = (
                f"http://chuck.mltech.info:8123/pawr/{self.reservation_name}"
            )
        elif reservation_type == "powder":
            self.reservation_name = f"POWDER-{self.reservation_number}"
            self.reservation_directory = POWDER_PATH / self.reservation_name
            self.output_url = (
                f"http://chuck.mltech.info:8123/powder/{self.reservation_name}"
            )
            self.radio_name_map = self._get_powder_name_map()
        else:
            raise ValueError("Not recognized reservation type")

        self.output_directory = self.reservation_directory / "output"
        self.output_directory.mkdir(parents=True, exist_ok=True)
        try:
            self.output_directory.chmod(0o777)
        except PermissionError:
            print("No permission for chmod")

        self.slack = slackweb.Slack(url=SLACK_URL)
        self.plot_points_max = 200_000
        self.start_time = self._get_reservation_start_time()
        self.start_dt = pd.to_datetime(self.start_time, unit="s")

    @classmethod
    def class_name(cls):
        """return classes name as a string"""
        return cls.__name__

    @abstractmethod
    def run(self) -> None:
        """run the job"""

    def _get_powder_name_map(self):
        """radio number to name mapping"""
        print(self.reservation_directory / "radio_map.json")
        list_mapping = pd.json_normalize(
            json.load((self.reservation_directory / "radio_map.json").open())
        ).set_index("node")
        return list_mapping.to_dict()["radio_name"]

    def _get_reservation_start_time(self):
        """get start time from rf_start_time.json"""
        for start_file in self.reservation_directory.glob("**/rf_start_time.json"):
            return json.load(start_file.open())

    def save_sns(self, fgr, name):
        """save and push figure to slack/gs"""
        file_name = f"{name}_{self.reservation_name}.png"
        path = self.output_directory / file_name
        plt.subplots_adjust(top=0.98)
        if hasattr(fgr, "fig"):
            fgr.fig.suptitle(self.reservation_name)
        print(f"saving image to {path}")
        fgr.savefig(str(path), bbox_inches="tight", facecolor="white")
        path.chmod(0o777)

        plt.close("all")
        return path

    def write_string_to_file(
        self, string, name=None, suffix=None, file_name=None, sub_folder=None
    ):
        """write string to file"""
        if file_name is None:
            file_name = f"{name}_{self.reservation_name}.{suffix}"

        if sub_folder is None:
            f_path = self.output_directory / file_name
        else:
            (self.output_directory / sub_folder).mkdir(parents=True, exist_ok=True)
            f_path = self.output_directory / sub_folder / file_name

        print(f"writing to {f_path}")
        with f_path.open("w", encoding="utf-8") as f:
            f.write(string)
        f_path.chmod(0o777)

    @staticmethod
    def setup_sns():
        """set sns settings"""
        sns.set_style("darkgrid")


class ReservationJobOperator(PythonOperator):
    """implement boilerplate required to create an airflow task from a
    Reservation Job"""

    def __init__(self, reservation_job: Type[ReservationJob], **kwargs):
        job_name = reservation_job.__name__
        super().__init__(
            task_id=job_name,
            provide_context=True,
            python_callable=run_job,
            op_kwargs={"reservation_job": reservation_job},
            **kwargs,
        )
