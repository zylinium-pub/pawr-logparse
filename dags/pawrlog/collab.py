"""process collab logs"""
import json
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

from .core import ReservationJob, plotting, jsoning

matplotlib.use("Agg")


class Collab(ReservationJob):
    """process enb log files"""

    def __init__(self, *args, **kwargs):
        self.df = None
        super().__init__(*args, **kwargs)

    def load_data(self):
        """load all data into objejct memory"""
        self.df = self._get_all_collab()

    def _get_all_collab(self):
        """get incumbent df"""
        pdfs = []
        json_paths = list(self.reservation_directory.glob("**/collab_raw.json"))
        print(json_paths)

        for json_path in json_paths:
            lines = [json.loads(line) for line in json_path.open() if "REPORT" in line]
            df = pd.io.json.json_normalize(lines)
            df["file_path"] = json_path.parent.name
            pdfs.append(df)
        if pdfs:
            pdf = pd.concat(pdfs)
        else:
            return

        pdf["time"] = (
            pdf["timestamp.seconds"].astype(int)
            + pdf["timestamp.picoseconds"].astype(int) * 1e-12
        )

        pdf["run_time"] = pdf["time"] - self.start_time
        try:
            pdf.loc[~pdf["incumbent_notify.data.power"].isna(), "metric"] = pdf.loc[
                ~pdf["incumbent_notify.data.power"].isna(),
                "incumbent_notify.data.power",
            ]
            pdf.loc[
                ~pdf["incumbent_notify.data.center_freq"].isna(), "center_freq"
            ] = pdf.loc[
                ~pdf["incumbent_notify.data.center_freq"].isna(),
                "incumbent_notify.data.center_freq",
            ].astype(
                int
            )

            pdf.loc[
                ~pdf["incumbent_notify.data.bandwidth"].isna(), "bandwidth"
            ] = pdf.loc[
                ~pdf["incumbent_notify.data.bandwidth"].isna(),
                "incumbent_notify.data.bandwidth",
            ].astype(
                int
            )
        except KeyError:
            print("no passive incumbent data")

        try:
            pdf.loc[
                ~pdf["incumbent_notify.data_active.center_freq"].isna(), "center_freq"
            ] = pdf.loc[
                ~pdf["incumbent_notify.data_active.center_freq"].isna(),
                "incumbent_notify.data_active.center_freq",
            ].astype(
                int
            )
            pdf.loc[
                ~pdf["incumbent_notify.data_active.bandwidth"].isna(), "bandwidth"
            ] = pdf.loc[
                ~pdf["incumbent_notify.data_active.bandwidth"].isna(),
                "incumbent_notify.data_active.bandwidth",
            ].astype(
                int
            )
            pdf.loc[
                ~pdf["incumbent_notify.data_active.inr"].isna(), "metric"
            ] = pdf.loc[
                ~pdf["incumbent_notify.data_active.inr"].isna(),
                "incumbent_notify.data_active.inr",
            ]
        except KeyError:
            print("no active incumbent data")

        return pdf

    @plotting
    def _plot_freq_bw(self):
        """plot frequency and bandwidth"""

        def plot_freq_bw(run_time, center_freq, bandwidth, color, label):
            plt.plot(run_time, center_freq, color=color, label=label)
            plt.plot(
                run_time, center_freq - bandwidth / 2, "--", color=color, label=label
            )
            plt.plot(
                run_time, center_freq + bandwidth / 2, "--", color=color, label=label
            )

        fgr = sns.FacetGrid(
            self.df,
            row="network_type.network_type",
            hue="file_path",
            height=3,
            aspect=4,
        )
        fgr = fgr.map(plot_freq_bw, "run_time", "center_freq", "bandwidth")
        fgr.add_legend()

        return fgr

    @plotting
    def _plot_incumbent_metric(self):
        """plot time series"""
        fgr = sns.FacetGrid(
            self.df,
            row="network_type.network_type",
            hue="file_path",
            height=3,
            aspect=4,
            sharey=False,
        )
        fgr = fgr.map(plt.plot, "run_time", "metric", ls="", marker=".", alpha=0.2)
        metric_median = self.df.metric.median()
        fgr = fgr.set(ylim=(metric_median - 10, metric_median + 10))
        fgr.add_legend()

        return fgr

    @jsoning
    def _data_90th_passive(self):
        """90th percentile passive incumbent reading"""
        return self.df[
            self.df["network_type.network_type"] == "INCUMBENT_PASSIVE"
        ].metric.quantile(q=0.95)

    @jsoning
    def _data_incumbent(self):
        """save incumbent data to json file"""
        return self.df.to_dict(orient="records")

    def run(self):
        """run this job"""
        self.load_data()
        if not self.df.empty:
            self._data_incumbent()
            self._data_90th_passive()
            self._plot_incumbent_metric()
            self._plot_freq_bw()
