"""Sends images to a Google Storage Bucket where they are hosted
so that they can be posted to Slack"""
import os
import uuid
from pathlib import Path
from time import sleep
import numpy as np
from google.cloud import storage
from .core import ReservationJob
from .config import GOOGLE_BUCKET_NAME, GOOGLE_CREDS_PATH, SLACK_DELAY

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = str(Path(GOOGLE_CREDS_PATH).expanduser())


class Slack(ReservationJob):
    """process traffic log files"""

    def _upload_png(self, path) -> str:
        """upload to gs, set perms to public, return public url"""

        storage_client = storage.Client()
        bucket = storage_client.bucket(GOOGLE_BUCKET_NAME)
        blob = bucket.blob(f"{uuid.uuid4().hex}_{path.name}")

        blob.upload_from_filename(str(path))

        blob.make_public()

        print(f"Blob {blob.name} is publicly accessible at {blob.public_url}")

        self.write_string_to_file(
            blob.public_url, file_name=path.with_suffix(".url").name, sub_folder="url"
        )

        return blob.public_url

    def _send_pngs(self):
        """send png files to slack"""
        pngs = sorted(list(self.output_directory.glob("*.png")))

        image_urls = []
        for png in pngs:
            image_urls.append(self._upload_png(png))

        for png, image_url in zip(pngs, image_urls):
            try:
                self.slack.notify(text=f"*{png.name}*")
                self.slack.notify(attachments=[{"title": "", "image_url": image_url}])
            except:
                print(f"Slack error.  Can't send {png.name}")

    def _send_file(self, suffix):
        """send files to slack"""
        files = self.output_directory.glob(f"*.{suffix}")
        for filef in sorted(files):
            try:
                self.slack.notify(text=f"*{filef.name}*")
                file_text = f"```{filef.read_text()}```"
                self.slack.notify(text=file_text)
            except:
                print(f"Slack error.  Can't send {filef.name}")

    def run(self):
        """run this job"""
        # backoff a random delay.  Needed because we may have multiple jobs
        # running in parallel and we do not want them to collide in our slack
        # feed
        sleep(np.random.rand() * SLACK_DELAY)
        self.slack.notify(text=f"Processing {SLACK_DELAY} * {self.reservation_name}*")
        self.slack.notify(text=f"Data at {self.output_url}")
        self._send_pngs()
        self._send_file("md")
