"""Airflow common functions"""
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from airflow.exceptions import AirflowSkipException


def conditionally_trigger(context, dag_run_obj):
    """This function decides whether or not to Trigger the remote DAG"""
    reservation_type, reservation_list = context["ti"].xcom_pull(
        task_ids="get_batch_reservations"
    )
    print(reservation_list)

    this_task_id = int(context["task_instance"].task_id.split("_")[-1])
    print(this_task_id)

    if this_task_id < len(reservation_list):
        dag_run_obj.payload = {
            "task_run_number": context["params"]["task_run_number"],
            "reservation": reservation_list[this_task_id],
            "reservation_type": reservation_type,
        }
        print(dag_run_obj)

        return dag_run_obj
    raise AirflowSkipException(f"Skipping DAG {this_task_id}")


def get_batch_reservations(get_batch_function, **context):
    """poll colosseum api to see what new reservations have completed"""
    this_ex_ts = context["execution_date"]
    prev_ex_ts = context["prev_execution_date"]
    next_ex_ts = context["next_execution_date"]
    print((prev_ex_ts, this_ex_ts, next_ex_ts))
    return get_batch_function(prev_ex_ts.timestamp(), this_ex_ts.timestamp())


def get_conditional_sub_dags(dag):
    """get the list of conditional sub dags"""

    subs = []
    for slot_number in range(20):
        subs.append(
            TriggerDagRunOperator(
                task_id=f"trigger_reservation_processing_{slot_number}",
                trigger_dag_id="process_reservation",
                python_callable=conditionally_trigger,
                params={"task_run_number": slot_number},
                dag=dag,
            )
        )
    return subs
