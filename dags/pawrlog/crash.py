"""check for crash"""
from .core import ReservationJob, jsoning


class Crash(ReservationJob):
    """process crash files"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.crash_paths = list(self.reservation_directory.glob("**/*.crash"))
        self.syslogs = list(self.reservation_directory.glob("**/syslog"))

    @jsoning
    def _crash_report(self):
        """print crash report"""
        return self.crash_paths

    def run(self):
        """run this job"""
        self._crash_report()
