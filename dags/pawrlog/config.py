"""configuration parameters"""
from pathlib import Path

SLACK_URL = (
    "https://hooks.slack.com/services/..."
)
TEST_SLACK_URL = (
    "https://hooks.slack.com/services/..."
)
GOOGLE_BUCKET_NAME = ""
GOOGLE_CREDS_PATH = "~/creds.json"

POLL_INTERVAL = 3

COLOSSEUM_USERNAME = ""
COLOSSEUM_KEY = ""

POWDER_PATH = Path("/data/powder")

ES_CLOUD_ID = ""
ES_API_KEY = ("", "")

ES_INDEX = "reservations"
ES_GOODPUT_INDEX = "goodput"
ES_PASSIVE_INDEX = "passive"

TEST_ES_INDEX = "reservations_test"

SLACK_DELAY = 220
TEST_SLACK_DELAY = 0

COLOSSEUM_HOST = "https://experiments.colosseum.net/"
COLOSSEUM_ENDPOINT = "api/v1/batch/jobs"

LOCAL_RSYNC_PATH = "/data/pawr/"
REMOTE_COLOSSEUM_PATH = "sc2-lz:/share/nas/[team_name]"
