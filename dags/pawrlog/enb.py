"""process eNobeB logs"""
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import control

from .core import ReservationJob, plotting
from .grok_patterns import PRB_PATTERN, RBG_PATTERN

matplotlib.use("Agg")


class ENB(ReservationJob):
    """process enb log files"""

    def __init__(self, *args, **kwargs):
        self.prb_dfs = None
        self.rbg_dfs = None
        super().__init__(*args, **kwargs)
        self.logs = list(self.reservation_directory.glob("**/enb.log"))
        print(self.logs)

    def load_data(self):
        """load all data into objejct memory"""
        self.prb_dfs = self._get_ul_prbs()
        self.rbg_dfs = self._get_dl_rbgs()
        print(self.rbg_dfs)
        self._get_prb_hist()
        self._get_rbg_hist()

    def _get_prb_hist(self):
        """compute prb histograms"""

        def transform(histogram, row):
            """increment historgram"""
            histogram[row.prb_low : row.prb_high] += 1

        return self._get_hist(self.prb_dfs, transform)

    def _get_rbg_hist(self):
        """compute rbg histograms"""

        def transform(histogram, row):
            """increment historgram"""
            histogram += row.rbg_array

        return self._get_hist(self.rbg_dfs, transform)

    @staticmethod
    def _get_hist(df, transform_function):
        """compute prb histograms"""
        rows_per_timestep = 1000
        for log in df:
            grams = []
            histogram = np.zeros(log["num"])
            last_sf = -1
            for row in log["df"].itertuples():
                sf_wrap = row.unwrapped_sf % rows_per_timestep
                transform_function(histogram, row)
                if sf_wrap < last_sf - 10:
                    grams.append(histogram * 1)
                    histogram = np.zeros(log["num"])
                last_sf = sf_wrap

            hist_stack = np.vstack(grams)
            log["histogram"] = hist_stack.T / rows_per_timestep

    def _get_log_data(self, ul, text_pattern, grok_pattern, special_fields=()):
        """generic enb parser"""
        print(special_fields)
        nof_rpb2nof_rbg = {6: 6, 15: 8, 25: 13, 50: 17, 75: 19, 100: 25}
        dfs = []
        for log in self.logs:
            out = []
            nof_prb = 100
            for line in log.open():
                if "Starting RX/TX thread nof_prb=" in line:
                    nof_prb = int(line.split("=")[1].split(",")[0])
                    nof_rbg = nof_rpb2nof_rbg[nof_prb]
                    print(nof_prb)
                if text_pattern in line:
                    match = grok_pattern.match(line)
                    if match is None:
                        print(line)
                        break
                    match["node_path"] = log.parent.name
                    for field, function in special_fields:
                        match[field] = function(match, nof_rbg)
                    out.append(match)
            df = pd.DataFrame(out)
            print(df)
            df["dt"] = (
                pd.to_datetime(
                    df.ts,
                    unit="us",
                )
                - self.start_dt
            )
            df["unwrapped_sf"] = control.unwrap(df.subframe * 1, 10_240).astype(int)
            dfs.append(
                {
                    "node_path": log,
                    "nof_prb": nof_prb,
                    "nof_rbg": nof_rbg,
                    "df": df,
                    "num": nof_prb if ul else nof_rbg,
                }
            )
            print(log, len(out))
        return dfs

    def _get_dl_rbgs(self):
        """parse enb.log DL resource block groups to pandas dataframe"""

        def rbg_hex_to_int_array(match, nof_rbg):
            """convert rbg hex to np int array"""
            bin_str = "{0:025b}".format(int(match["rbg"], 16))[:nof_rbg]
            return np.array(list(bin_str), int)

        return self._get_log_data(
            False, "rbg=0x", RBG_PATTERN, (("rbg_array", rbg_hex_to_int_array),)
        )

    def _get_ul_prbs(self):
        """parse enb.log to pandas dataframe"""
        return self._get_log_data(True, "prb=(", PRB_PATTERN)

    @plotting
    def _plot_prbs(self):
        """plot PRB histogram"""
        for ix, dfs_value in enumerate(self.prb_dfs + self.rbg_dfs):
            fig = plt.figure(figsize=(20, 8.5 * dfs_value["nof_prb"] / 100))
            plt.imshow(
                np.log10(dfs_value["histogram"]),
                aspect="auto",
                interpolation=None,
                origin="lower",
                vmin=-3,
                vmax=0,
                extent=[
                    dfs_value["df"].dt.min().total_seconds(),
                    dfs_value["df"].dt.max().total_seconds(),
                    0,
                    dfs_value["num"],
                ],
            )
            plt.xlim([0, None])
            plt.ylabel("PRB/RGB index")
            plt.xlabel("Time index (seconds)")
            plt.title(
                "Plot of % PRB/RGB usage per s.  Color in log scale.\n"
                f"{dfs_value['node_path']}"
            )
            plt.colorbar()

            yield (f"enb_{ix}", fig)

    def run(self):
        """run this job"""
        self.load_data()
        self._plot_prbs()
