"""process radio.conf logs"""
import json
import pandas as pd

from .core import ReservationJob, tabling


class RadioConf(ReservationJob):
    """process enb log files"""

    def __init__(self, *args, **kwargs):
        self.df = None
        super().__init__(*args, **kwargs)

    def load_data(self):
        """load all data into objejct memory"""
        self.df = self._get_all_radio_conf()

    def _get_all_radio_conf(self):
        """get radio.conf df"""
        json_paths = list(self.reservation_directory.glob("**/radio.conf"))
        print(json_paths)

        radio_confs = []
        for json_path in json_paths:
            radio_conf = json.load(json_path.open())
            radio_conf["radio_path"] = json_path.parent.name
            radio_confs.append(radio_conf)
        pdf = pd.json_normalize(radio_confs).sort_values("node")
        if "args.team" in pdf:
            pdf = pdf.sort_values(["args.team", "node"])
        if "args.masking_enabled" in pdf:
            pdf["args.masking_enabled"] = pdf["args.masking_enabled"].fillna("False")

        return pdf

    @tabling
    def _print_table(self):
        """print table"""
        cols = [
            "radio_path",
            "args.enb.n_prb",
            "args.prb_guard",
            "args.masking_enabled",
            "args.rbg_guard",
            "args.rf.ul_freq",
            "args.rf.dl_freq",
            "args.rf.tx_gain",
            "args.rf.rx_gain",
            "args.team",
            "node",
        ]
        cols_in_df = set(self.df.columns)
        cols_to_print = list(set(cols).intersection(cols_in_df))
        return self.df[cols_to_print]

    def run(self):
        """run this job"""
        self.load_data()
        self._print_table()
