"""Colosseum API methods"""
import os
from typing import List
import requests

from requests.auth import HTTPBasicAuth

from .config import (
    COLOSSEUM_USERNAME,
    COLOSSEUM_KEY,
    COLOSSEUM_HOST,
    COLOSSEUM_ENDPOINT,
    LOCAL_RSYNC_PATH,
    REMOTE_COLOSSEUM_PATH,
)


def get_batch_reservations_(start_time: float, stop_time: float) -> List[str]:
    """poll colosseum api to see what new reservations have completed"""

    # get reservation from last two days. Need to grab older reservations due
    # to how colosseum api works.
    res = requests.get(
        COLOSSEUM_HOST + COLOSSEUM_ENDPOINT,
        params=dict(start_time=start_time - 60 * 60 * 48, stop_time=stop_time),
        verify=False,
        auth=HTTPBasicAuth(COLOSSEUM_USERNAME, COLOSSEUM_KEY),
    )

    reservation_data = res.json()
    print(reservation_data)

    # loop through reservations and keep any that are in our time interval.
    res_ids = []
    for completed in reservation_data["complete"]:
        res_info = completed["reservation_info"][0]
        if res_info["end"] >= start_time and res_info["end"] < stop_time:
            res_ids.append(res_info["res_id"])
    return "colosseum", res_ids


def rsync_reservations_(
    reservation_ids: List[str],
    local_path=LOCAL_RSYNC_PATH,
    remote_path=REMOTE_COLOSSEUM_PATH,
    files_to_download=(
        "rf_start_time.json",
        "traffic.log",
        "enb.log",
        "radio.conf",
        "cap7.fc32",
        "collab.log",
        "collab_client.log",
        "collab_raw.json",
        "*metric*",
        "*crash",
        "syslog",
    ),
):
    """rsync input reservations"""

    if not reservation_ids:
        return

    reservation_folders = [f"RESERVATION-{res_id}" for res_id in reservation_ids]
    # "JUNK" is a hacky filler word to handle empty reservation_folders
    reservations_re = "{{{},JUNK}}".format(",".join(reservation_folders))
    remote = f"{remote_path}/{reservations_re}"
    includes = "".join(
        [" --include='{}' ".format(_file) for _file in files_to_download]
    )
    c_str = f"""rsync -ravmz {includes} --chmod=ugo=rwX --exclude='*.*'  {remote} {local_path}"""
    print(c_str)
    os.system(c_str)
    for reservation in reservation_folders:
        os.system(f"chmod -R 777 {local_path}/{reservation}")
