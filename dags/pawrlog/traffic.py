"""process traffic logs"""
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

from .core import ReservationJob, plotting, tabling, jsoning

matplotlib.use("Agg")


class Traffic(ReservationJob):
    """process traffic log files"""

    def __init__(self, *args, **kwargs):
        self.df = None
        self.df_missing = None
        self.df_received = None
        self.df_goodput = None
        self.df_sent_good_put = None
        super().__init__(*args, **kwargs)

    def load_data(self):
        """load all data into objejct memory"""
        self.df = self._get_traffic_data()
        self.df_missing, self.df_received = self._get_miss_rx()
        self.df_goodput = self._get_goodput()
        self.df_sent_good_put = self._get_sent_good_put()

    def _get_traffic_data(self):
        """get data into pandas"""
        logs = list(self.reservation_directory.glob("**/traffic.log"))
        print(list(logs))
        dfs = []
        for log in logs:
            print(log)
            dfs.append(
                pd.read_csv(
                    log,
                    header=None,
                    names=[
                        "Timestamp",
                        "Team",
                        "Direction",
                        "Flow_Number",
                        "Source",
                        "Dest",
                        "Packet_Num",
                        "Packet_Size",
                        "CRC",
                        "Rx_CRC",
                    ],
                )
            )
        df = pd.concat(dfs)
        df["dt"] = pd.to_datetime(df["Timestamp"]) - self.start_dt
        df["ts"] = df.dt.map(lambda x: x.total_seconds())
        df["ts_int"] = df.ts.astype(int)
        df["Source"] = df["Source"].map(lambda id_: self.radio_name_map.get(id_, id_))
        df["Dest"] = df["Dest"].map(lambda id_: self.radio_name_map.get(id_, id_))
        df["link"] = df["Source"].astype(str) + "->" + df["Dest"].astype(str)
        return df

    def _get_miss_rx(self):
        """get missing and rx data"""

        df = self.df.drop(columns=["Timestamp", "CRC", "Rx_CRC"])

        df_s = df[df.Direction == "SEND"]
        df_r = df[df.Direction == "RECV"]

        df_m = df_s.merge(
            df_r,
            on=["Team", "Packet_Num", "link", "Flow_Number", "Packet_Size"],
            how="left",
        )
        df_m = df_m.rename(columns={"ts_x": "ts"})

        df_missing = df_m[df_m["Direction_y"].isna()]
        df_received = df_m[~df_m["Direction_y"].isna()]
        df_received["latency"] = (
            df_received["ts_y"] - df_received["ts"]
        ).values.astype(int) * 1e-9
        df_received["ts_int"] = df_received.ts.astype(int)
        df_missing["ts_int"] = df_missing.ts.astype(int)
        return df_missing, df_received

    def _get_goodput(self):
        """get goodput df"""
        missing_count = (
            self.df_missing.groupby(["Team", "link", "Flow_Number"])
            .Packet_Size.count()
            .reset_index()
            .rename(columns={"Packet_Size": "missing"})
        )
        rx_count = (
            self.df_received.groupby(["Team", "link", "Flow_Number"])
            .Packet_Size.count()
            .reset_index()
            .rename(columns={"Packet_Size": "received"})
        )

        missing_rx_df = (
            rx_count.merge(
                missing_count, on=["Team", "link", "Flow_Number"], how="outer"
            )
            .fillna(0)
            .set_index(["Team", "link", "Flow_Number"])
        )

        missing_rx_df["goodput"] = (
            100
            * missing_rx_df.received
            / (missing_rx_df.received + missing_rx_df.missing)
        )

        return missing_rx_df

    def _get_sent_good_put(self):
        """get goodput with sentput df"""
        df_all = (
            self.df[(self.df.Direction == "SEND")]
            .groupby(["ts_int", "link", "Team", "Flow_Number"])
            .Packet_Size.sum()
            * 8
            / 1e6
        )
        df_all = df_all.reset_index()
        df_all["key"] = "sentput"

        df_1 = (
            self.df_received.groupby(
                ["ts_int", "link", "Team", "Flow_Number"]
            ).Packet_Size.sum()
            * 8
            / 1e6
        )
        df_1 = df_1.reset_index()
        df_1["key"] = "goodput"

        df = pd.concat([df_1, df_all])
        df = df.rename(columns={"Packet_Size": "Throughput_Mbps"})
        df["source"] = df.link.str.split("->").pipe(np.vstack)[:, 0]
        df["destination"] = df.link.str.split("->").pipe(np.vstack)[:, 1]

        return df

    @plotting
    def _plot_timeseries(self):
        """plot time series"""
        fgr = sns.FacetGrid(
            self.df,
            row="link",
            col="Team",
            hue="Direction",
            height=3,
            aspect=3,
            sharey=False,
        )
        fgr = fgr.map(plt.plot, "ts", "Packet_Num", ls="", marker=".", alpha=0.02)
        fgr = fgr.add_legend().set(xlim=(0, None))

        return fgr

    @plotting
    def _plot_badput(self):
        """plot missing packets"""
        fgr = sns.FacetGrid(
            self.df_missing, row="link", col="Team", height=3, aspect=3, sharey=False
        )
        fgr = fgr.map(plt.plot, "ts", "Packet_Num", ls="", marker=".", alpha=0.02)
        fgr = fgr.add_legend().set(xlim=(0, None))

        return fgr

    @plotting
    def _plot_latency(self):
        """plot latency"""
        fgr = sns.FacetGrid(
            self.df_received, row="link", col="Team", height=3, aspect=3, sharey=True
        )
        fgr = (
            fgr.map(plt.plot, "ts", "latency")
            .set(ylim=(0, 5))
            .set(xlim=(0, None))
            .add_legend()
        )

        return fgr

    @plotting
    def _plot_throughput(self):
        """plot sendput and goodput"""
        fgr = sns.FacetGrid(
            self.df_sent_good_put,
            row="link",
            col="Team",
            hue="key",
            height=3,
            aspect=3,
            sharey=True,
        )
        fgr = (
            fgr.map(plt.plot, "ts_int", "Throughput_Mbps")
            .set(ylim=(0, None))
            .set(xlim=(0, None))
            .add_legend()
        )
        return fgr

    @tabling
    def _text_goodput(self):
        """print text and html of goodput"""
        return self.df_goodput.reset_index()

    @jsoning
    def _data_mean_goodput(self):
        """save mean goodput to json file"""
        return self.df_goodput.goodput.mean()

    @jsoning
    def _data_link_goodput(self):
        """save mean goodput to json file"""
        return self.df_sent_good_put.to_dict(orient="records")

    def run(self):
        """run this job"""
        self.load_data()
        self._data_link_goodput()
        self._data_mean_goodput()
        self._plot_timeseries()
        self._plot_throughput()
        self._plot_latency()
        self._text_goodput()
