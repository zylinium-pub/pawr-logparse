"""Send reservation data to Elastic search"""
import json
from datetime import datetime

import numpy as np
from elasticsearch import Elasticsearch, helpers

from .core import ReservationJob
from .config import ES_CLOUD_ID, ES_API_KEY, ES_INDEX


class Elastic(ReservationJob):
    """Send data to ES"""

    def __init__(self, *args, **kwargs):
        self.elasticsearch = Elasticsearch(
            cloud_id=ES_CLOUD_ID,
            api_key=ES_API_KEY,
        )
        self.run_info = []
        self.target_index = ES_INDEX
        super().__init__(*args, **kwargs)

    def get_file_data(self, prefix, default=0):
        """get file data from output json"""
        file_paths = list(self.output_directory.glob(f"{prefix}*.json"))
        if not file_paths:
            return default
        return json.load(file_paths[0].open())

    def _get_data_from_node_name(self, node_name):
        """return branch info from node_name"""
        print(node_name)
        if self.reservation_type == "colosseum":
            (
                org,
                base_name,
                srslte_version,
                ansible_branch,
                collaboration_version,
                soe_version,
                srn_name,
                _,
            ) = node_name.split("-")
            return {
                "build_data": {
                    "org": org,
                    "base_name": base_name,
                    "srslte_version": srslte_version,
                    "ansible_branch": ansible_branch,
                    "collaboration_version": collaboration_version,
                    "soe_version": soe_version,
                    "srn_name": srn_name,
                    "node_name": node_name,
                }
            }
        return {
            "build_data": {
                "node_name": node_name,
            }
        }

    def _compose_data_dict(self):
        """create dict of data to send to ES"""

        out = []

        passive_level = self.get_file_data("Collab__data_90th_passive_", 0)
        goodput = self.get_file_data("Traffic__data_mean_goodput_", 0)

        if np.isnan(passive_level) or np.isnan(goodput):
            return None

        url_dict = {}
        for url in self.output_directory.glob("**/*.url"):
            if self.reservation_type == "colosseum":
                split_term = "RESERVATION"
            else:
                split_term = self.reservation_type.upper()

            url_name = url.name.split("_" + split_term)[0]
            url_dict[url_name] = url.read_text()

        for traffic_path in self.reservation_directory.glob("**/traffic.log"):
            node_path = traffic_path.parent

            data_dict = json.load((node_path / "radio.conf").open())["args"]

            gitsha = (node_path / "gitsha").read_text()
            gitsha = json.loads(
                '{"' + gitsha.replace("\n", '","').replace(": ", '":"')[:-2] + "}"
            )
            data_dict.update({"gitsha": gitsha})

            data_dict.update(self._get_data_from_node_name(node_path.name))

            data_dict.update(
                {
                    "time_epoch": self.start_time,
                    "time": datetime.utcfromtimestamp(self.start_time).isoformat(),
                    "reservation": self.reservation_name,
                    "reservation_number": self.reservation_number,
                    "reservation_type": self.reservation_type,
                    "reservation_url": self.output_url,
                    "passive_level_90": passive_level,
                    "goodput": goodput,
                }
            )
            data_dict.update({"urls": url_dict})
            out.append(data_dict)

        return out

    def _send_bulk_data_to_es(self):
        """send data dict to ES"""
        actions = [
            {
                "_index": self.target_index,
                "_source": node,
            }
            for node in self.run_info
        ]

        print(helpers.bulk(self.elasticsearch, actions))

    def load_data(self):
        """load all data into objejct memory"""
        self.run_info = self._compose_data_dict()
        self._send_bulk_data_to_es()

    def run(self):
        """run this job"""
        self.load_data()
