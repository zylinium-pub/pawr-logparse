"""Send reservation data to Elastic search"""
from datetime import datetime

from .config import ES_GOODPUT_INDEX
from .elastic import Elastic


class ElasticTraffic(Elastic):
    """Send goodput data to ES"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.target_index = ES_GOODPUT_INDEX

    def _compose_data_dict(self):
        """create dict of data to send to ES"""

        out = []

        goodput = self.get_file_data("Traffic__data_link_goodput_", 0)

        for row in goodput:
            row.update(
                {
                    "time_epoch": self.start_time,
                    "time": datetime.utcfromtimestamp(self.start_time).isoformat(),
                    "reservation": self.reservation_name,
                    "reservation_number": self.reservation_number,
                    "reservation_type": self.reservation_type,
                    "reservation_url": self.output_url,
                }
            )
            out.append(row)

        return out
