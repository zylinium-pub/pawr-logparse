"""grok patterns"""
from pygrok import Grok

# 1606848460606035 [MAC ] [I] [ 5146] SCHED: Msg3 tx rnti=0x5e, cc=0, pid=2, dci=(0,0), prb=(0,3), n_rtx=0, tbs=7, bsr=0 (0-7)
PRB_PATTERN = Grok(
    """
%{INT:ts:int} 
\[%{WORD:component}\s*\] \[I\] 
\[\s*%{NUMBER:subframe:int}\] 
%{WORD:message_1}: 
%{WORD:message_2} 
%{WORD:message_3} 
rnti=%{WORD:rnti}, 
cc=%{NUMBER:cc:int}, 
pid=%{NUMBER:pid:int}, 
dci=\(%{INT:dci_low:int},%{INT:dci_high:int}\), 
prb=\(%{INT:prb_low:int},%{INT:prb_high:int}\), 
n_rtx=%{NUMBER:n_rtx:int}, 
tbs=%{NUMBER:tbs:int}, 
bsr=%{NUMBER:bsr:int} 
\(%{INT:interval_low:int}-%{INT:interval_high:int}\)
""".replace(
        "\n", ""
    )
)

# 1606848465570127 [PHY1] [I] [10110] PDCCH: cc=0, f=1, cce=14, L=1, rbg=0x1000000, pid=0, mcs={13}, ndi={1}, tpc_pucch=1, tti_tx_dl=10114
RBG_PATTERN = Grok(
    """
%{INT:ts:int} 
\[%{WORD:component}\s*\] \[I\] 
\[\s*%{NUMBER:subframe:int}\] 
%{WORD:message_1}: 
cc=%{NUMBER:cc:int}, 
f=%{NUMBER:f:int}, 
cce=\s*%{NUMBER:cce:int}, 
L=%{NUMBER:L:int}, 
rbg=%{WORD:rbg}, 
pid=%{NUMBER:pid:int}, 
mcs=\{%{NUMBER:mcs:int}\}, 
ndi=\{%{NUMBER:ndi:int}\}, 
tpc_pucch=%{NUMBER:tpc_pucch:int}, 
tti_tx_dl=\s*%{NUMBER:tti_tx_dl:int}
""".replace(
        "\n", ""
    )
)
