"""DAG to process a reservation"""
# pylint: disable=W0104
from datetime import datetime, timedelta

from airflow.models import DAG
from airflow.operators.bash_operator import BashOperator

from pawrlog.traffic import Traffic
from pawrlog.metrics import Metrics
from pawrlog.enb import ENB
from pawrlog.slack import Slack
from pawrlog.collab import Collab
from pawrlog.radio_conf import RadioConf
from pawrlog.core import ReservationJobOperator
from pawrlog.elastic import Elastic
from pawrlog.elastic_traffic import ElasticTraffic
from pawrlog.elastic_passive import ElasticPassive

dag = DAG(
    "process_reservation",
    default_args={
        "start_date": datetime.utcnow() - timedelta(minutes=60),
    },
    schedule_interval=None,
    is_paused_upon_creation=False,
)


t_print_reservation = BashOperator(
    task_id="print_reservation",
    bash_command='echo "Reservation:  '
    '{{ dag_run.conf["reservation"] if dag_run else "" }}" ',
    dag=dag,
)

t_traffic = ReservationJobOperator(reservation_job=Traffic, dag=dag)
t_metrics = ReservationJobOperator(reservation_job=Metrics, dag=dag)
t_enb = ReservationJobOperator(reservation_job=ENB, dag=dag)
t_collab = ReservationJobOperator(reservation_job=Collab, dag=dag)
t_radio_conf = ReservationJobOperator(reservation_job=RadioConf, dag=dag)
t_elastic = ReservationJobOperator(reservation_job=Elastic, dag=dag)
t_elastic_traffic = ReservationJobOperator(reservation_job=ElasticTraffic, dag=dag)
t_elastic_passive = ReservationJobOperator(reservation_job=ElasticPassive, dag=dag)

t_slack = ReservationJobOperator(
    reservation_job=Slack,
    dag=dag,
    trigger_rule="all_done",
)

t_print_reservation >> [
    t_traffic,
    t_metrics,
    t_enb,
    t_collab,
    t_radio_conf,
] >> t_slack >> [t_elastic, t_elastic_traffic, t_elastic_passive]
